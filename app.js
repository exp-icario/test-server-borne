const express = require('express');
const { WebSocketServer } = require('ws');
const http = require('http');

const app = express();
const PORT = 3000;

app.use(express.json());
app.use(express.static('public'));

let connections = [];

const server = http.createServer(app);
const wss = new WebSocketServer({ server });

wss.on('connection', function connection(ws, req) {
  let ip = req.socket.remoteAddress;
  if (ip.includes('::ffff:')) {
    ip = ip.split('::ffff:')[1];
  }

  ws.send("test message")
  connections.push({ ws, ip }); // Store the connection and IP
  console.log(`New WebSocket connection: ${ip}`);

  ws.on('close', () => {
    // Remove the connection when it's closed
    connections = connections.filter(conn => conn.ws !== ws);
    console.log(`WebSocket connection closed: ${ip}`);
  });

  ws.on('message', function message(data) {
    console.log("received : " + data)

    if (data == "STARTING") {
      ws.close()
      console.log("Connections:", JSON.stringify(connections.map(conn => ({ ip: conn.ip })), null, 2));
    }
  })
});


app.get('/api', (req, res) => {
  if (connections.length > 0) {
    // Select a random connection
    const randomIndex = Math.floor(Math.random() * connections.length);
    const randomConnection = connections[randomIndex].ws;

    // Send a message to the selected WebSocket connection
    randomConnection.send('STARTUP');

    res.json({ message: 'Sent a startup message to a random WebSocket client.' });
    console.log("Sent startup message to a random WebSocket client.");
  } else {
    res.status(404).json({ message: 'No WebSocket clients connected.' });
    console.log("Attempted to send message, but no WebSocket clients are connected.");
  }
});


server.listen(PORT, () => {
  console.log(`Server listening on http://localhost:${PORT}`);
});
