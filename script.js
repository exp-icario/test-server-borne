document.getElementById('sendRequestBtn').addEventListener('click', function () {
  fetch('http://example.com/api/endpoint', {
    method: 'GET', // or 'POST'
    headers: {
      'Content-Type': 'application/json',
      // Additional headers if needed
    },
    // body: JSON.stringify(data) // if method is POST
  })
    .then(response => response.json()) // assuming server responds with JSON
    .then(data => console.log(data))
    .catch(error => console.error('Error:', error));
});
